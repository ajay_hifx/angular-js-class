angular.module('notesApp', [])
	.controller('noteController', ['$scope', function ($scope) {
		$scope.notes = ['Note 1', 'Note 2', 'Note 3'];
		$scope.newNote = '';

		$scope.addNote = function ($event) {
			if ($scope.newNote != '') {
				$scope.notes.push($scope.newNote);
			}
			$scope.newNote = '';
		}

		$scope.deleteNote = function ($index, $event) {
			$scope.notes.splice($index, 1)
			$event.preventDefault();
		}
	}]);