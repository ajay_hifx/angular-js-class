angular.module('employeeApp', [])
	.controller('employeeController', ['$scope', function ($scope) {
		$scope.submit = function () {
			//console.log($scope.myForm.email.$error);
		}
	}])








































	.directive('uniqueEmail', ['$http', '$q', function ($http, $q) {
		return {
			require: 'ngModel', 
			link : function ($scope, elem, attrs, ctrl) {
				ctrl.$asyncValidators.uniqueEmail = function (modelValue, viewValue) {
					if (modelValue) {
						var def = $q.defer();
						$http.get('emailchecker.php?email=' + modelValue).success(function (data) {
							if (data == "exists") {
								def.reject();
							} else {
								def.resolve();
							}
						});
						return def.promise;
					} else {
						return $q.when();
					}
				}
			}
		}
	}]);