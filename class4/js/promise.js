$(document).ready(function () {
	function myFunc(name, milliseconds) {
		var d = $.Deferred();
		setTimeout(function () {
			d.resolve('Hi ' + name);
		}, milliseconds);
		return d.promise();
	}

	$.when(myFunc("1", 2000), myFunc("2", 4000), myFunc("3", 3000), myFunc("4", 5000)).done(function (v1, v2, v3, v4) {
		console.log(v1);
		console.log(v2);
		console.log(v3);
		console.log(v4);
	});
});