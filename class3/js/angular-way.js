angular.module('employeeApp', [])
	.controller('employeeController', ['$scope', 'CLIENT_ID', 'myService', function ($scope, clientId, myService) {
		$scope.employees = [];
		//alert(clientId);
		//alert(myService.mangatholi);
		console.log(myService);
		$scope.hireEmployee = function () {
			if ($scope.name && $scope.salary && ($scope.score > 0 && $scope.score < 100)) {
				$scope.employees.push({
					'name' : $scope.name,
					'salary' : $scope.salary,
					'score' : $scope.score,
				});
				$scope.name = '';
				$scope.salary = '';
				$scope.score = '';
			}
		}
	}])

	.filter('grade', function() {
		return function(input) {
			input = parseInt(input);
			if (input < 100 && input > 80)
		        return 'A';
		    else if (input < 80 && input > 60)
		        return 'B';
		    else if (input < 60 && input > 40)
		        return 'C';
		    else
		        return 'D';
		};
	})

	.directive('datepicker', function() {
		return {
			restrict: 'A',
			link: function($scope, element, attrs) {
				var picker = new Pikaday({
					field: element.get(0),
					format: attrs.datepickerFormat
				});

				attrs.$observe('datepickerMax', function () {
					if (attrs.datepickerMax) {
						picker.setMaxDate(moment(attrs.datepickerMax).toDate());
					}
				});

				attrs.$observe('datepickerMin', function () {
					if (attrs.datepickerMin) {
						picker.setMinDate(moment(attrs.datepickerMin).toDate());
					}
				});
			}
		};
	})

	.directive('catpicker', function() {
		return {
			restrict: 'E',
			templateUrl: 'catpicker.template.html',
			link: function($scope, element, attrs) {
				$scope.cats = [
					{name : 'Blue Cat', selected : false},
					{name : 'Green Cat', selected : false},
					{name : 'Red Cat', selected : false},
					{name : 'Magenta Cat', selected : false},
					{name : 'Old Cat', selected : true},
					{name : 'Grumpy Cat', selected : true},
				];
			}
		};
	})



	.value('CLIENT_ID', 'abc')

	.service('myService', function () {
		this.mangatholi = 'thengakkola';
		this.tholi = function () {
			return this.mangatholi
		}
	})

	.factory('myFactory', function () {
		return [1, 2]
	});