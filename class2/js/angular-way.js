angular.module('employeeApp', [])
	.controller('employeeController', ['$scope', function ($scope) {
		$scope.employees = [];

		$scope.hireEmployee = function () {
			if ($scope.name && $scope.salary && ($scope.score > 0 && $scope.score < 100)) {
				$scope.employees.push({
					'name' : $scope.name,
					'salary' : $scope.salary,
					'score' : $scope.score,
				});
				$scope.name = '';
				$scope.salary = '';
				$scope.score = '';
			}
		}

		$scope.deleteNote = function ($index, $event) {
			$scope.notes.splice($index, 1)
			$event.preventDefault();
		}
	}])

	.filter('grade', function() {
		return function(input) {
			input = parseInt(input);
			if (input < 100 && input > 80)
		        return 'A';
		    else if (input < 80 && input > 60)
		        return 'B';
		    else if (input < 60 && input > 40)
		        return 'C';
		    else
		        return 'D';
		};
	});