$(document).ready(function() {
	init();

	function init() {
		$.each(['Note 1', 'Note 2', 'Note 3'], function (index, note) {
			addNote(note);
		});
	}

	$('#button').click(function(e){
		newNote = $('#textField').val();
		if (newNote.length > 0) {
			addNote(newNote);
			clearTextField();
			setCharCount(0);
		}
		e.preventDefault();
	});

	$('#textField').keyup(function(e) {
		setCharCount($('#textField').val().length);
	});

	$('#notelist').on('click', 'li', function(e){
		$(this).remove();
		if ($('#notelist li').length == 0) {
			showMessage();
		}
		e.preventDefault();
	});

	function showMessage() {
		$('#noNotes').show();
	}

	function hideMessage() {
		$('#noNotes').hide();
	}

	function drawAllNotes(notesArray) {
		for (i = 0; i < notesArray.length; i++) {
			addNote(notesArray[i]);
		}
	}

	function setCharCount (count) {
		$('#charCount').text(count);
	}

	function clearTextField() {
		$('#textField').val('');
	}

	function addNote(text) {
		$('#notelist').append('<li><a href="#">' + text + '</a></li>');
		hideMessage();
	}
});